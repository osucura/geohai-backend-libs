const Cache = require('./cache/cache');
const AsyncCache = require('./cache/asyncCache');
const Cluster = require('./cache/cluster');
const AsyncCluster = require('./cache/asyncCluster');


function getValidatedClusters(clusters) {
  if (Array.isArray(clusters) && clusters.length < 3) {
      throw new Error('cache cluster should have more than 3');
  }
  return clusters;
}

const functions = {
  createCache: function (clusters) {
    let validClusters = getValidatedClusters(clusters);
    
    if (Array.isArray(validClusters)) {
      return new Cluster(validClusters);
    } else {
      return new Cache(validClusters.port, validClusters.host);
    }
  },

  createAsyncCache: function (clusters,usedMockCache) {

    let validClusters = getValidatedClusters(clusters);

    if (Array.isArray(validClusters)) {
      return new AsyncCluster(validClusters);
    } else {
      const asyncCache = new AsyncCache(validClusters.port, validClusters.host);
      if ( usedMockCache ){
        asyncCache.useMockCache();
      }
      return asyncCache;
    }

  },
};

module.exports = functions;
