# ESIGN cache module

Cache sync functions are continuables (just like the ones used with gen-run).
Async functions like invalidate,set,... return a promise.

```javascript
const [port, host] = [6379, 'localhost'];
const cache = require('cache').createCache(port, host);

function* getFromCache(group, key) {
  const value = yield cache.get(group, key);
  return value;
}.awaitify();

function* bulkGetFromCache() {
  const groupsAndKeys = [
    {
      group: 'aaa',
      key: 'register',
      ttl: 10, // look at this
      value: 'ahmad'
    },
    {
      group: 'aaa',
      key: 'register2',
      value: {ahmad: 'ahmad'}
    }
  ];
  yield cache.bulkSet(groupsAndKeys);
  const values = yield cache.bulkGet(groupsAndKeys); //values = ['ahmad', {ahmad: 'ahmad'}]
  return values;
}.awaitify();
```

Available Methods:
================
- get
- bulkGet
- set
- setSync
- bulkSet
- invalidate
- invalidateSync
- All native ioredis methods (except those overridden above)
