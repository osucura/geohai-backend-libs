process.env.NODE_ENV = 'TEST';

const AsyncCache = require('../cache/asyncCache');

const asyncCache = new AsyncCache(6379, 'localhost');

const sleep = (value) => {
  const promise = new Promise((resolve) => {
    setTimeout(resolve, value);
  });

  return promise;
};

describe('asyncCache', function () {
  beforeEach(async () => {
    asyncCache.reset();
  });

  it('should test asyncCache set sync function', async () => {
    const complexObject = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    await asyncCache.set('ali', 'elahi', complexObject, 1);
    const value = await asyncCache.get('ali', 'elahi');
    expect(value).toEqual(complexObject);
  });

  it('should test asyncCache set async function', async () => {
    const complexObject = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    asyncCache.set('ali', 'lotfi', complexObject, 1);
    await sleep(5);
    const value = await asyncCache.get('ali', 'lotfi');
    expect(value).toEqual(complexObject);
  });

  it('should test asyncCache invalidate sync function', async () => {
    const complexObject = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    await asyncCache.set('mamad', 'izadi123123', complexObject, 1);
    const valueAfterSet = await asyncCache.get('mamad', 'izadi123123');
    expect(valueAfterSet).toEqual(complexObject);
    await asyncCache.invalidate('mamad', 'izadi123123');
    const valueAfterInvalidate = await asyncCache.get('mamad', 'izadi123123');
    expect(valueAfterInvalidate).toBeUndefined();
  });

  it('should test asyncCache invalidate async function', async () => {
    const complexObject = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    await asyncCache.set('urate', 'uranus@@@', complexObject, 1);
    const valueAfterSet = await asyncCache.get('urate', 'uranus@@@');
    expect(valueAfterSet).toEqual(complexObject);
    asyncCache.invalidate('urate', 'uranus@@@');
    await sleep(5);
    const valueAfterInvalidate = await asyncCache.get('urate', 'uranus@@@');
    expect(valueAfterInvalidate).toBeUndefined();
  });

  it('should test bulkSet/Get function', async () => {
    const complexObjects = [
      {asdf: 22, asdf2: 'asdf2'}, {asdf: 22, asdf1: 'asdf1', location: {latitude: 2, longitude: '52'}}
    ];
    await asyncCache.bulkSet([
      {
        group: 'ali',
        key: 'mashhadi1',
        value: complexObjects[0],
        ttl: 1
      },
      {
        group: 'ali',
        key: 'mashhadi2',
        value: complexObjects[1],
        ttl: 1
      }
    ]);
    const values = await asyncCache.bulkGet([
      {
        group: 'ali',
        key: 'mashhadi1',
      },
      {
        group: 'ali',
        key: 'mashhadi2',
      }
    ]);
    expect(values[0]).toEqual(complexObjects[0]);
    expect(values[1]).toEqual(complexObjects[1]);
  });

  it('should test asyncCache ttl on set for async set', async () => {
    const complexObject = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    asyncCache.set('mamad', 'izadi123123', complexObject, 1);
    await sleep(5);
    const valueAfterSet = await asyncCache.get('mamad', 'izadi123123');
    expect(valueAfterSet).toEqual(complexObject);
    await sleep(1000);
    const valueAfterTimeout = await asyncCache.get('mamad', 'izadi123123');
    expect(valueAfterTimeout).toBeUndefined();
  });

  it('should test asyncCache ttl on set for sync set', async () => {
    const complexObject = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    await asyncCache.set('mamad', 'izadi123123', complexObject, 1);
    const valueBeforeTimeout = await asyncCache.get('mamad', 'izadi123123');
    await sleep(995);
    expect(valueBeforeTimeout).toEqual(complexObject);
    await sleep(10);
    const valueAfterTimeout = await asyncCache.get('mamad', 'izadi123123');
    expect(valueAfterTimeout).toBeUndefined();
  });

  it('should test bulkSet/Get function with ttl on set', async () => {
    const complexObjects = [
      {asdf: 22, asdf2: 'asdf2'}, {
        asdf: 22,
        asdf1: 'asdf1',
        location: {latitude: 2, longitude: '52'}
      }, {s: new Date().toISOString()}
    ];
    await asyncCache.bulkSet([
      {
        group: 'mj',
        key: '12',
        value: complexObjects[0],
        ttl: 10
      },
      {
        group: 'rajab',
        key: '13',
        value: complexObjects[1],
        ttl: 2
      },
      {
        group: 'karaj',
        key: 'club',
        value: complexObjects[2],
        ttl: 10
      }
    ]);
    const valuesAfterSet = await asyncCache.bulkGet([
      {
        group: 'mj',
        key: '12',
      },
      {
        group: 'rajab',
        key: '13',
      },
      {
        group: 'karaj',
        key: 'club',
      }
    ]);
    expect(valuesAfterSet[0]).toEqual(complexObjects[0]);
    expect(valuesAfterSet[1]).toEqual(complexObjects[1]);
    expect(valuesAfterSet[2]).toEqual(complexObjects[2]);
    await sleep(2005);
    const valuesAfterTimeout = await asyncCache.bulkGet([
      {
        group: 'mj',
        key: '12',
      },
      {
        group: 'rajab',
        key: '13',
      },
      {
        group: 'karaj',
        key: 'club',
      }
    ]);
    expect(valuesAfterTimeout[0]).toEqual(complexObjects[0]);
    expect(valuesAfterTimeout[1]).toBeNull();
    expect(valuesAfterTimeout[2]).toEqual(complexObjects[2]);
  });

  it('should test asyncCache lpush / rpop function', async () => {
    const complexObject = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    const complexObject2 = {set: 3, get: 4, location: {latitude: 2, longitude: '52'}, sari: 12 * 44};
    const complexObject3 = 'salam';
    await asyncCache.lpush('mohamad', 'jalali', complexObject);
    await asyncCache.lpush('mohamad', 'jalali', complexObject2);
    await asyncCache.lpush('mohamad', 'jalali', complexObject3);
    const value = await asyncCache.rpop('mohamad', 'jalali');
    const value2 = await asyncCache.rpop('mohamad', 'jalali');
    const value3 = await asyncCache.rpop('mohamad', 'jalali');
    expect(value).toEqual(complexObject);
    expect(value2).toEqual(complexObject2);
    expect(value3).toEqual(complexObject3);
  });

  it('should test asyncCache bulkLpush / bulkRpop function', async () => {
    const complexObject1 = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    const complexObject2 = {name: 'masoud', family: 'kazemi'};
    await asyncCache.bulkLpush([
      {
        group: 'mohamad',
        key: 'jalali',
        value: complexObject1,
      },
      {
        group: 'mohamad',
        key: 'jalali',
        value: complexObject2,
      }
    ]);
    const values = await asyncCache.bulkRpop([
      {
        group: 'mohamad',
        key: 'jalali',
      },
      {
        group: 'mohamad',
        key: 'jalali',
      }
    ]);
    expect(values[0]).toEqual(complexObject1);
    expect(values[1]).toEqual(complexObject2);
  });

});
