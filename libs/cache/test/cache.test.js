process.env.NODE_ENV = 'TEST';

const awaitify = require('awaitify');
const Cache = require('../cache/cache');

const cache = new Cache(6379, 'localhost');

const sleep = (value) => awaitify.cb(callback => setTimeout(callback, value));

describe('cache', function () {
  beforeEach(function* () {
    yield cache.reset();
  }.awaitify());

  it('should test cache set sync function', function* () {
    const complexObject = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    yield cache.setSync('ali', 'elahi', complexObject, 1);
    const value = yield cache.get('ali', 'elahi');
    expect(value).toEqual(complexObject);
  }.awaitify());

  it('should test cache set async function', function* () {
    const complexObject = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    cache.set('ali', 'lotfi', complexObject, 1);
    yield sleep(5);
    const value = yield cache.get('ali', 'lotfi');
    expect(value).toEqual(complexObject);
  }.awaitify());

  it('should test cache invalidate sync function', function* () {
    const complexObject = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    yield cache.setSync('mamad', 'izadi123123', complexObject, 1);
    const valueAfterSet = yield cache.get('mamad', 'izadi123123');
    expect(valueAfterSet).toEqual(complexObject);
    yield cache.invalidateSync('mamad', 'izadi123123');
    const valueAfterInvalidate = yield cache.get('mamad', 'izadi123123');
    expect(valueAfterInvalidate).toBeUndefined();
  }.awaitify());

  it('should test cache invalidate async function', function* () {
    const complexObject = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    yield cache.setSync('urate', 'uranus@@@', complexObject, 1);
    const valueAfterSet = yield cache.get('urate', 'uranus@@@');
    expect(valueAfterSet).toEqual(complexObject);
    cache.invalidate('urate', 'uranus@@@');
    yield sleep(5);
    const valueAfterInvalidate = yield cache.get('urate', 'uranus@@@');
    expect(valueAfterInvalidate).toBeUndefined();
  }.awaitify());

  it('should test bulkSet/Get function', function* () {
    const complexObjects = [
      {asdf: 22, asdf2: 'asdf2'}, {asdf: 22, asdf1: 'asdf1', location: {latitude: 2, longitude: '52'}}
    ];
    yield cache.bulkSet([
      {
        group: 'ali',
        key: 'mashhadi1',
        value: complexObjects[0],
        ttl: 1
      },
      {
        group: 'ali',
        key: 'mashhadi2',
        value: complexObjects[1],
        ttl: 1
      }
    ]);
    const values = yield cache.bulkGet([
      {
        group: 'ali',
        key: 'mashhadi1',
      },
      {
        group: 'ali',
        key: 'mashhadi2',
      }
    ]);
    expect(values[0]).toEqual(complexObjects[0]);
    expect(values[1]).toEqual(complexObjects[1]);
  }.awaitify());

  it('should test cache ttl on set for async set', function* () {
    const complexObject = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    cache.set('mamad', 'izadi123123', complexObject, 1);
    yield sleep(5);
    const valueAfterSet = yield cache.get('mamad', 'izadi123123');
    expect(valueAfterSet).toEqual(complexObject);
    yield sleep(1000);
    const valueAfterTimeout = yield cache.get('mamad', 'izadi123123');
    expect(valueAfterTimeout).toBeUndefined();
  }.awaitify());

  it('should test cache ttl on set for sync set', function* () {
    const complexObject = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    yield cache.setSync('mamad', 'izadi123123', complexObject, 1);
    const valueBeforeTimeout = yield cache.get('mamad', 'izadi123123');
    yield sleep(995);
    expect(valueBeforeTimeout).toEqual(complexObject);
    yield sleep(10);
    const valueAfterTimeout = yield cache.get('mamad', 'izadi123123');
    expect(valueAfterTimeout).toBeUndefined();
  }.awaitify());

  it('should test bulkSet/Get function with ttl on set', function* () {
    const complexObjects = [
      {asdf: 22, asdf2: 'asdf2'}, {
        asdf: 22,
        asdf1: 'asdf1',
        location: {latitude: 2, longitude: '52'}
      }, {s: new Date().toISOString()}
    ];
    yield cache.bulkSet([
      {
        group: 'mj',
        key: '12',
        value: complexObjects[0],
        ttl: 10
      },
      {
        group: 'rajab',
        key: '13',
        value: complexObjects[1],
        ttl: 2
      },
      {
        group: 'karaj',
        key: 'club',
        value: complexObjects[2],
        ttl: 10
      }
    ]);
    const valuesAfterSet = yield cache.bulkGet([
      {
        group: 'mj',
        key: '12',
      },
      {
        group: 'rajab',
        key: '13',
      },
      {
        group: 'karaj',
        key: 'club',
      }
    ]);
    expect(valuesAfterSet[0]).toEqual(complexObjects[0]);
    expect(valuesAfterSet[1]).toEqual(complexObjects[1]);
    expect(valuesAfterSet[2]).toEqual(complexObjects[2]);
    yield sleep(2005);
    const valuesAfterTimeout = yield cache.bulkGet([
      {
        group: 'mj',
        key: '12',
      },
      {
        group: 'rajab',
        key: '13',
      },
      {
        group: 'karaj',
        key: 'club',
      }
    ]);
    expect(valuesAfterTimeout[0]).toEqual(complexObjects[0]);
    expect(valuesAfterTimeout[1]).toBeNull();
    expect(valuesAfterTimeout[2]).toEqual(complexObjects[2]);
  }.awaitify());

  it('should test cache lpush / rpop function', function* () {
    const complexObject = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    const complexObject2 = {set: 3, get: 4, location: {latitude: 2, longitude: '52'}, sari: 12 * 44};
    const complexObject3 = 'salam';
    cache.lpush('mohamad', 'jalali', complexObject);
    cache.lpush('mohamad', 'jalali', complexObject2);
    cache.lpush('mohamad', 'jalali', complexObject3);
    const value = yield cache.rpop('mohamad', 'jalali');
    const value2 = yield cache.rpop('mohamad', 'jalali');
    const value3 = yield cache.rpop('mohamad', 'jalali');
    expect(value).toEqual(complexObject);
    expect(value2).toEqual(complexObject2);
    expect(value3).toEqual(complexObject3);
  }.awaitify());

  it('should test cache bulkLpush / bulkRpop function', function* () {
    const complexObject1 = {set: 2, get: 2, location: {latitude: 2, longitude: '52'}, sari: 12 * 2};
    const complexObject2 = {name: 'masoud', family: 'kazemi'};
    yield cache.bulkLpush([
      {
        group: 'mohamad',
        key: 'jalali',
        value: complexObject1,
      },
      {
        group: 'mohamad',
        key: 'jalali',
        value: complexObject2,
      }
    ]);
    const values = yield cache.bulkRpop([
      {
        group: 'mohamad',
        key: 'jalali',
      },
      {
        group: 'mohamad',
        key: 'jalali',
      }
    ]);
    expect(values[0]).toEqual(complexObject1);
    expect(values[1]).toEqual(complexObject2);
  }.awaitify());

});
