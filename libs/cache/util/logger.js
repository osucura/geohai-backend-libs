const config = require('../config');

const loggerConfig = config.get('logger');
const logger = require('logger')(loggerConfig);

module.exports = logger;
