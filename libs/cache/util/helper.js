'use strict';
const path = require('path');
const fs = require('fs');

const functions = {
  extractScript(script) {
    const address = script.file;
    const lua = fs.readFileSync(address).toString();

    let command = path.basename(script.file);
    command = command.substr(0, command.length - path.extname(command).length);

    if (script.command) {
      command = script.command;
    }

    return {
      numberOfKeys: script.numberOfKeys,
      command,
      lua,
    };
  },
  addScripts(cache, scripts, isAsync){
    scripts.forEach(scriptItem => {
      const script = functions.extractScript(scriptItem);
      const client = isAsync ? cache.client: cache;
      client.defineCommand(script.command, {
        numberOfKeys: script.numberOfKeys,
        lua: script.lua,
      });
      const command = script.command;
      if (isAsync && !(command in cache)) {
        cache[command] = (...args) => {
          return cache.client[command].apply(cache.client, args);
        };
      }
    });
  }
};
module.exports = functions;