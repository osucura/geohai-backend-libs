const Redis = require('ioredis');
const mockRedis = require('./mockRedis');
const errorCodes = require('../config/errorCodes.json');
const logger = require('../util/logger');
const validator = require('validator');
const helper = require('../util/helper');

const processEnv = process.env.NODE_ENV;
const DEFAULT_CACHE_TTL = 60 * 60 * 24;

const privates = {
  parseJsonValue: function (jsonValue) {
    try {
      let value = JSON.parse(jsonValue);
      return value;
    } catch (error) {
      return jsonValue;
    }
  }
};

class Cache extends Redis {
  constructor(port, host) {
    validator.isNumber(port, 'port');
    validator.notNullOrEmpty(host, 'host');
    super(port, host);
  }
  addScripts(scripts){
    helper.addScripts(this, scripts);
  }

  reset() {
    if (this.isMocked) {
      return mockRedis.flushdb();
    }
  }

  useMockCache() {
    this.isMocked = true;
    for (const functionName of Object.keys(mockRedis)) {
      Redis.prototype[functionName] = mockRedis[functionName];
    }
  }

  getRedisKey(group, key) {
    if (key === 0 || key)
      return group + '_' + key;
    else
      return group;
  }

  get(group, key) {
    return (callback) => {
      let start = new Date().getTime();
      try {
        let redisKey = this.getRedisKey(group, key);
        super.get(redisKey, function (error, jsonValue) {
          if (error) {
            logger.codeString(error, errorCodes.REDIS_ERROR);
            return callback(error);
          }
          try {
            if (jsonValue) {
              let value = privates.parseJsonValue(jsonValue);
              callback(null, value);
            } else {
              callback();
            }
          } catch (error) {
            callback();
            logger.codeString(error, errorCodes.REDIS_ERROR);
            logger.error(error);
          }
        });
      } catch (error) {
        callback();
        logger.tag(error, 'cacheModule::cache::sugarCache');
        logger.error(error);
      }
    };
  }

  incr(key) {
    return (callback) => {
      try {
        super.incr(key, function (error, value) {
          if (error) {
            logger.codeString(error, errorCodes.REDIS_ERROR);
            return callback(error);
          }
          callback(null, value);
        });
      } catch (error) {
        logger.tag(error, 'cacheModule::cache::sugarCache');
        callback(error);
      }
    };
  }

  geopos(key, member) {
    return (callback) => {
      try {
        super.geopos(key, member, function (error, value) {
          if (error) {
            logger.codeString(error, errorCodes.REDIS_ERROR);
            return callback(error);
          }
          try {
            if (value) {
              callback(null, value);
            } else {
              callback();
            }
          } catch (error) {
            callback();
            logger.codeString(error, errorCodes.REDIS_ERROR);
            logger.error(error);
          }
        });

      } catch (error) {
        callback();
        logger.codeString(error, errorCodes.REDIS_ERROR);
        logger.error(error);
      }
    };
  }

  set(group, key, value, timeToLive) {
    let jsonValue = JSON.stringify(value);
    let redisKey = this.getRedisKey(group, key);

    if (!timeToLive || isNaN(timeToLive)) {
      let error = new Error();
      logger.codeString(error, errorCodes.TTL_IS_MANDATORY);
      if (processEnv === 'PRODUCTION') {
        timeToLive = DEFAULT_CACHE_TTL;
        logger.error(error);
      } else {
        throw error;
      }
    }

    return super.set(redisKey, jsonValue, 'EX', timeToLive);
  }

  setSync(group, key, value, timeToLive) {
    return (callback) => {
      try {
        let jsonValue = JSON.stringify(value);
        let redisKey = this.getRedisKey(group, key);

        if (!timeToLive || isNaN(timeToLive)) {
          let error = new Error();
          logger.codeString(error, errorCodes.TTL_IS_MANDATORY);
          if (processEnv === 'PRODUCTION') {
            timeToLive = DEFAULT_CACHE_TTL;
            logger.error(error);
          } else {
            return callback(error);
          }
        }

        super.set(redisKey, jsonValue, 'EX', timeToLive)
          .then(function () {
            callback();
          })
          .catch(function (error) {
            logger.codeString(error, errorCodes.REDIS_ERROR);
            callback(error);
          });
      } catch (error) {
        logger.codeString(error, errorCodes.REDIS_ERROR);
        callback(error);
      }
    };
  }

  lpush(group, key, value) {
    try {
      let redisKey = this.getRedisKey(group, key);
      return super.lpush(redisKey, JSON.stringify(value));
    } catch (error) {
      logger.codeString(error, errorCodes.REDIS_ERROR);
      logger.error(error);
      return new Promise();
    }
  }

  rpop(group, key) {
    return (callback) => {
      try {
        let redisKey = this.getRedisKey(group, key);
        super.rpop(redisKey, function (error, jsonValue) {
          if (error) {
            logger.codeString(error, errorCodes.REDIS_ERROR);
            logger.error(error);
            return callback();
          }
          try {
            if (jsonValue) {
              let value = privates.parseJsonValue(jsonValue);
              callback(null, value);
            } else {
              callback();
            }
          } catch (error) {
            callback();
            logger.codeString(error, errorCodes.REDIS_ERROR);
            logger.error(error);
          }
        });
      } catch (error) {
        callback();
        logger.tag(error, 'cacheModule::cache::sugarCache');
        logger.error(error);
      }
    };
  }

  invalidateSync(group, key) {
    return (callback) => {
      let redisKey = this.getRedisKey(group, key);
      super.del(redisKey)
        .then(function () {
          callback(null);
        })
        .catch(function (error) {
          logger.codeString(error, errorCodes.REDIS_ERROR);
          callback(error);
        });
    };
  }

  invalidate(group, key) {
    let redisKey = this.getRedisKey(group, key);
    return super.del(redisKey);
  }

  bulkGet(groupsAndKeysArray) {
    return (callback) => {
      try {
        let pipeline = super.pipeline();
        for (let groupAndKey of groupsAndKeysArray) {
          let redisKey = this.getRedisKey(groupAndKey.group, groupAndKey.key);
          pipeline.get(redisKey);
        }
        let bulkResult = [];
        pipeline.exec(function (error, pipelineExecutionResults) {
          for (let result of pipelineExecutionResults) {
            if (result[0]) {
              let error = new Error();
              logger.codeString(error, errorCodes.REDIS_ERROR)
              logger.data(error, result[0]);
              logger.error(error);
              bulkResult.push(null);
            } else {
              if (result[1]) {
                bulkResult.push(JSON.parse(result[1]));
              } else {
                bulkResult.push(null);
              }
            }
          }
          callback(null, bulkResult);
        });
      } catch (error) {
        callback();
        logger.codeString(error, errorCodes.REDIS_ERROR);
        logger.error(error);
      }
    };
  }

  bulkSet(groupsAndKeysAndValuesArray) {
    return (callback) => {
      let pipeline = super.pipeline();
      for (let groupAndKeyAndValue of groupsAndKeysAndValuesArray) {
        let redisKey = this.getRedisKey(groupAndKeyAndValue.group, groupAndKeyAndValue.key);
        let value = groupAndKeyAndValue.value;
        let ttl = groupAndKeyAndValue.ttl;
        if (!ttl || isNaN(ttl)) {
          let error = new Error();
          logger.codeString(error, errorCodes.TTL_IS_MANDATORY);
          if (processEnv === 'PRODUCTION') {
            ttl = DEFAULT_CACHE_TTL;
            logger.error(error);
          } else {
            return callback(error)
          }
        }
        pipeline.set(redisKey, JSON.stringify(value), 'EX', ttl);
      }
      pipeline.exec(function (error, pipelineExecutionResults) {
        for (let result of pipelineExecutionResults) {
          if (result[0]) {
            let error = new Error();
            error.codeString(error, errorCodes.REDIS_ERROR);
            logger.data(error, {error: result[0]});
            logger.error(error);
          }
        }
        callback();
      });
    };
  }

  bulkLpush(groupsAndKeysAndValuesArray) {
    return (callback) => {
      try {
        let pipeline = super.pipeline();
        for (let groupAndKeyAndValue of groupsAndKeysAndValuesArray) {
          let redisKey = this.getRedisKey(groupAndKeyAndValue.group, groupAndKeyAndValue.key);
          let value = groupAndKeyAndValue.value;
          pipeline.lpush(redisKey, JSON.stringify(value));
        }
        pipeline.exec(function (error, pipelineExecutionResults) {
          for (let result of pipelineExecutionResults) {
            if (result[0]) {
              let error = new Error();
              error.codeString(error, errorCodes.REDIS_ERROR);
              logger.data(error, {error: result[0]});
              logger.error(error);
            }
          }
          callback();
        });
      } catch (error) {
        callback();
        logger.codeString(error, errorCodes.REDIS_ERROR);
        logger.error(error);
      }
    };
  }

  bulkRpop(groupsAndKeysArray) {
    return (callback) => {
      try {
        let pipeline = super.pipeline();
        for (let groupAndKey of groupsAndKeysArray) {
          let redisKey = this.getRedisKey(groupAndKey.group, groupAndKey.key);
          pipeline.rpop(redisKey);
        }
        let bulkResult = [];
        pipeline.exec(function (error, pipelineExecutionResults) {
          for (let result of pipelineExecutionResults) {
            if (result[0]) {
              let error = new Error();
              logger.codeString(error, errorCodes.REDIS_ERROR);
              logger.data(error, result[0]);
              logger.error(error);
              bulkResult.push(null);
            } else {
              if (result[1]) {
                bulkResult.push(JSON.parse(result[1]));
              } else {
                bulkResult.push(null);
              }
            }
          }
          callback(null, bulkResult);
        });
      } catch (error) {
        callback();
        logger.codeString(error, errorCodes.REDIS_ERROR);
        logger.error(error);
      }
    };
  }
}

module.exports = Cache;
