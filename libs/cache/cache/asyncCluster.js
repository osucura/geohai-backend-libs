const Redis = require('ioredis');
const mockRedis = require('./mockRedis');
const errorCodes = require('../config/errorCodes.json');
const logger = require('../util/logger');
const validator = require('validator');
const helper = require('../util/helper');

const processEnv = process.env.NODE_ENV;
const DEFAULT_CACHE_TTL = 60*60*24;

const privates = {
  getTimeDiffUpToNow: (start) => {
    const now = new Date().getTime();
    return now - start;
  },

  parseJsonValue: (jsonValue) => {
    try {
      const value = JSON.parse(jsonValue);
      return value;
    } catch (error) {
      return jsonValue;
    }
  },

  validateTimeToLive: (timeToLive) => {
    if (!timeToLive || isNaN(timeToLive)) {
      let error = new Error();
      logger.codeString(error, errorCodes.TTL_IS_MANDATORY);
      if (processEnv === 'PRODUCTION') {
        timeToLive = DEFAULT_CACHE_TTL;
        logger.error(error);
      } else {
        throw error;
      }
    }
    return timeToLive;
  }
};

class AsyncCluster {
  constructor(clusters) {
    for (let clusterConfig of clusters) {
      validator.isNumber(clusterConfig.port, 'port');
      validator.notNullOrEmpty(clusterConfig.host, 'host');
    }

    this.client = new Redis.Cluster(clusters);
    for (const command of this.client.getBuiltinCommands()) {
      if (!(command in this)) {
        this[command] = (...args) => {
          return this.client[command].apply(this.client, args);
        };
      }
    }
  }

  addScripts(scripts){
    helper.addScripts(this, scripts, true);
  }

  reset() {
    if (this.isMocked) {
      return this.client.flushdb();
    }
  }

  useMockCache() {
    this.isMocked = true;
    this.client = mockRedis;
  }

  getRedisKey(group, key) {
    if (key === 0 || key)
      return group + '_' + key;
    else
      return group;
  }

  async get(group, key) {
    try {
      const redisKey = this.getRedisKey(group, key);
      const jsonValue = await this.client.get(redisKey);
      if (jsonValue) {
        const value = privates.parseJsonValue(jsonValue);
        return value;
      }
    } catch (error) {
      logger.codeString(error, errorCodes.REDIS_ERROR);
      logger.error(error);
    }
  }

  async incr(key) {
    try {
      return await this.client.incr(key);
    } catch (error) {
      logger.codeString(error, errorCodes.REDIS_ERROR);
      throw error;
    }
  }

  async geopos(key, member) {
    try {
      const value = await this.client.geopos(key, member);
      return value;
    } catch (error) {
      logger.codeString(error, errorCodes.REDIS_ERROR);
      logger.error(error);
    }
  }

  async set(group, key, value, timeToLive) {
    timeToLive = privates.validateTimeToLive(timeToLive);
    const jsonValue = JSON.stringify(value);
    const redisKey = this.getRedisKey(group, key);

    await this.client.set(redisKey, jsonValue, 'EX', timeToLive);
  }

  async invalidate(group, key) {
    const redisKey = this.getRedisKey(group, key);
    await this.client.del(redisKey);
  }

  async bulkGet(groupsAndKeysArray) {
    const bulkResult = [];

    for (let groupAndKey of groupsAndKeysArray) {
      const redisKey = this.getRedisKey(groupAndKey.group, groupAndKey.key);
      const result = await this.client.get(redisKey);
      if (result) {
        bulkResult.push(JSON.parse(result));
      } else {
        bulkResult.push(null);
      }
    }

    return bulkResult;
  }

  async bulkSet(groupsAndKeysAndValuesArray) {
    for (let groupAndKeyAndValue of groupsAndKeysAndValuesArray) {
      const redisKey = this.getRedisKey(groupAndKeyAndValue.group, groupAndKeyAndValue.key);
      const value = groupAndKeyAndValue.value;
      let ttl = privates.validateTimeToLive(groupAndKeyAndValue.ttl);
      await this.client.set(redisKey, JSON.stringify(value), 'EX', ttl);
    }
  }
}

module.exports = AsyncCluster;
