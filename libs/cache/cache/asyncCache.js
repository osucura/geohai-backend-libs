const Redis = require('ioredis');
const mockRedis = require('./mockRedis');
const errorCodes = require('../config/errorCodes.json');
const logger = require('../util/logger');
const validator = require('validator');
const helper = require('../util/helper');

const processEnv = process.env.NODE_ENV;
const DEFAULT_CACHE_TTL = 60 * 60 * 24;

const privates = {
  getTimeDiffUpToNow: (start) => {
    const now = new Date().getTime();
    return now - start;
  },

  parseJsonValue: (jsonValue) => {
    try {
      let value = JSON.parse(jsonValue);
      return value;
    } catch (error) {
      return jsonValue;
    }
  },

  validateTimeToLive: (timeToLive) => {
    if (!timeToLive || isNaN(timeToLive)) {
      let error = new Error();
      logger.codeString(error, errorCodes.TTL_IS_MANDATORY);
      if (processEnv === 'PRODUCTION') {
        timeToLive = DEFAULT_CACHE_TTL;
        logger.error(error);
      } else {
        throw error;
      }
    }
    return timeToLive;
  }
};

class AsyncCache {
  constructor(port, host) {
    validator.isNumber(port, 'port');
    validator.notNullOrEmpty(host, 'host');

    this.client = new Redis(port, host);
    for (const command of this.client.getBuiltinCommands()) {
      if (!(command in this)) {
        this[command] = (...args) => {
          return this.client[command].apply(this.client, args);
        };
      }
    }
  }

  addScripts(scripts){
    helper.addScripts(this, scripts, true);
  }

  reset() {
    if (this.isMocked) {
      return this.client.flushdb();
    }
  }

  useMockCache() {
    this.isMocked = true;
    this.client = mockRedis;
  }

  getRedisKey(group, key) {
    if (key === 0 || key)
      return group + '_' + key;
    else
      return group;
  }

  async get(group, key) {
    try {
      const redisKey = this.getRedisKey(group, key);
      const jsonValue = await this.client.get(redisKey);
      if (jsonValue) {
        const value = privates.parseJsonValue(jsonValue);
        return value;
      }
    } catch (error) {
      logger.tag(error, 'cacheModule::cache::sugarCache');
      logger.error(error);
    }
  }

  async incr(key) {
    try {
      return await this.client.incr(key);
    } catch (error) {
      logger.tag(error, 'cacheModule::cache::sugarCache');
      throw error;
    }
  }

  async geopos(key, member) {
    try {
      const value = await this.client.geopos(key, member);
      return value;
    } catch (error) {
      logger.codeString(error, errorCodes.REDIS_ERROR);
      logger.error(error);
    }
  }

  async set(group, key, value, timeToLive) {
    timeToLive = privates.validateTimeToLive(timeToLive);

    const jsonValue = JSON.stringify(value);
    const redisKey = this.getRedisKey(group, key);

    await this.client.set(redisKey, jsonValue, 'EX', timeToLive);
  }

  async lpush(group, key, value) {
    try {
      const redisKey = this.getRedisKey(group, key);
      await this.client.lpush(redisKey, JSON.stringify(value));
    } catch (error) {
      logger.codeString(error, errorCodes.REDIS_ERROR);
      logger.error(error);
    }
  }

  async rpop(group, key) {
    try {
      const redisKey = this.getRedisKey(group, key);
      const jsonValue = await this.client.rpop(redisKey);
      if (jsonValue) {
        const value = privates.parseJsonValue(jsonValue);
        return value;
      }
    } catch (error) {
      logger.tag(error, 'cacheModule::cache::sugarCache');
      logger.error(error);
    }
  }

  async invalidate(group, key) {
    const redisKey = this.getRedisKey(group, key);
    await this.client.del(redisKey);
  }

  async bulkGet(groupsAndKeysArray) {
    try {
      const pipeline = this.client.pipeline();

      for (let groupAndKey of groupsAndKeysArray) {
        const redisKey = this.getRedisKey(groupAndKey.group, groupAndKey.key);
        pipeline.get(redisKey);
      }

      let bulkResult = [];

      const pipelineExecutionResults = await pipeline.exec();

      for (let result of pipelineExecutionResults) {
        if (result[0]) {
          let error = new Error();
          logger.codeString(error, errorCodes.REDIS_ERROR)
          logger.data(error, result[0]);
          logger.error(error);
          bulkResult.push(null);
        } else {
          if (result[1]) {
            bulkResult.push(JSON.parse(result[1]));
          } else {
            bulkResult.push(null);
          }
        }
      }
      return bulkResult;
    } catch (error) {
      logger.codeString(error, errorCodes.REDIS_ERROR);
      logger.error(error);
    }
  }

  async bulkSet(groupsAndKeysAndValuesArray) {
    const pipeline = this.client.pipeline();

    for (let groupAndKeyAndValue of groupsAndKeysAndValuesArray) {
      const redisKey = this.getRedisKey(groupAndKeyAndValue.group, groupAndKeyAndValue.key);
      const value = groupAndKeyAndValue.value;
      const ttl = privates.validateTimeToLive(groupAndKeyAndValue.ttl);
      pipeline.set(redisKey, JSON.stringify(value), 'EX', ttl);
    }

    try {
      const pipelineExecutionResults = await pipeline.exec();

      for (let result of pipelineExecutionResults) {
        if (result[0]) {
          let error = new Error();
          error.codeString(error, errorCodes.REDIS_ERROR);
          logger.data(error, {error: result[0]});
          logger.error(error);
        }
      }
    } catch (error) {
      logger.codeString(error, errorCodes.REDIS_ERROR);
      logger.error(error);
    }
  }

  async bulkLpush(groupsAndKeysAndValuesArray) {
    try {
      const pipeline = this.client.pipeline();

      for (let groupAndKeyAndValue of groupsAndKeysAndValuesArray) {
        const redisKey = this.getRedisKey(groupAndKeyAndValue.group, groupAndKeyAndValue.key);
        const value = groupAndKeyAndValue.value;
        pipeline.lpush(redisKey, JSON.stringify(value));
      }

      const pipelineExecutionResults = await pipeline.exec();

      for (let result of pipelineExecutionResults) {
        if (result[0]) {
          let error = new Error();
          error.codeString(error, errorCodes.REDIS_ERROR);
          logger.data(error, {error: result[0]});
          logger.error(error);
        }
      }
    } catch (error) {
      logger.codeString(error, errorCodes.REDIS_ERROR);
      logger.error(error);
    }
  }

  async bulkRpop(groupsAndKeysArray) {
    try {
      const pipeline = this.client.pipeline();

      for (let groupAndKey of groupsAndKeysArray) {
        let redisKey = this.getRedisKey(groupAndKey.group, groupAndKey.key);
        pipeline.rpop(redisKey);
      }

      let bulkResult = [];

      const pipelineExecutionResults = await pipeline.exec();

      for (let result of pipelineExecutionResults) {
        if (result[0]) {
          let error = new Error();
          logger.codeString(error, errorCodes.REDIS_ERROR);
          logger.data(error, result[0]);
          logger.error(error);
          bulkResult.push(null);
        } else {
          if (result[1]) {
            bulkResult.push(JSON.parse(result[1]));
          } else {
            bulkResult.push(null);
          }
        }
      }

      return bulkResult;
    } catch (error) {
      logger.codeString(error, errorCodes.REDIS_ERROR);
      logger.error(error);
    }
  }

  async zrem(key,member) {
    try{
      const rmResult = await this.client.zrem(key,member);

      return rmResult;
    } catch (error) {
      logger.tag(error, 'cacheModule::cache::zrem');
      throw error;
    }
  }
}

module.exports = AsyncCache;
