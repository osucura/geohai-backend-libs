const Promise = require('bluebird');
const MockRedis = require('ioredis-mock');
MockRedis.Promise = Promise;

const cache = new MockRedis({});

cache.blpop = async (key) => {
  while (true) {
    const lastElement = await cache.lpop(key);
    if (!!lastElement || lastElement === 0) {
      return lastElement;
    }
    await Promise.delay(10);
  }
};

module.exports = cache;
