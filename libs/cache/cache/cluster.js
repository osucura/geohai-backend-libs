const Redis = require('ioredis');
const mockRedis = require('./mockRedis');
const errorCodes = require('../config/errorCodes.json');
const logger = require('../util/logger');
const validator = require('validator');
const helper = require('../util/helper');

const processEnv = process.env.NODE_ENV;
const DEFAULT_CACHE_TTL = 60*60*24;

const privates = {
  parseJsonValue: function (jsonValue) {
    try {
      let value = JSON.parse(jsonValue);
      return value;
    } catch (error) {
      return jsonValue;
    }
  }
};

class Cluster extends Redis.Cluster {
  constructor(clusters) {
    for (let clusterConfig of clusters) {
      validator.isNumber(clusterConfig.port, 'port');
      validator.notNullOrEmpty(clusterConfig.host, 'host');
    }
    super(clusters);
  }
  addScripts(scripts){
    helper.addScripts(this, scripts);
  }


  reset() {
    if (this.isMocked) {
      return mockRedis.flushdb();
    }
  }

  useMockCache() {
    this.isMocked = true;
    for (const functionName of Object.keys(mockRedis)) {
      Redis.Cluster.prototype[functionName] = mockRedis[functionName];
    }
  }

  getRedisKey(group, key) {
    if (key === 0 || key)
      return group + '_' + key;
    else
      return group;
  }

  get(group, key) {
    return (callback) => {
      try {
        let redisKey = this.getRedisKey(group, key);
        super.get(redisKey, function (error, jsonValue) {
          if (error) {
            logger.codeString(error, errorCodes.REDIS_ERROR);
            return callback(error);
          }
          try {
            if (jsonValue) {
              let value = privates.parseJsonValue(jsonValue);
              callback(null, value);
            } else {
              callback();
            }
          } catch (error) {
            callback();
            logger.codeString(error, errorCodes.REDIS_ERROR);
            logger.error(error);
          }
        });
      } catch (error) {
        callback();
        logger.codeString(error, errorCodes.REDIS_ERROR);
        logger.error(error);
      }
    };
  }

  incr(key) {
    return (callback) => {
      try {
        super.incr(key, function (error, value) {
          if (error) {
            logger.codeString(error, errorCodes.REDIS_ERROR);
            return callback(error);
          }
          callback(null, value);
        });
      } catch (error) {
        logger.codeString(error, errorCodes.REDIS_ERROR);
        callback(error);
      }
    };
  }

  geopos(key, member) {
    return (callback) => {
      try {
        super.geopos(key, member, function (error, value) {
          if (error) {
            logger.codeString(error, errorCodes.REDIS_ERROR);
            return callback(error);
          }
          try {
            if (value) {
              callback(null, value);
            } else {
              callback();
            }
          } catch (error) {
            callback();
            logger.codeString(error, errorCodes.REDIS_ERROR);
            logger.error(error);
          }
        });

      } catch (error) {
        callback();
        logger.codeString(error, errorCodes.REDIS_ERROR);
        logger.error(error);
      }
    };
  }

  set(group, key, value, timeToLive) {
    let jsonValue = JSON.stringify(value);
    let redisKey = this.getRedisKey(group, key);
    if (!timeToLive || isNaN(timeToLive)) {
      let error = new Error();
      logger.codeString(error, errorCodes.TTL_IS_MANDATORY);
      if (processEnv === 'PRODUCTION') {
        timeToLive = DEFAULT_CACHE_TTL;
        logger.error(error);
      } else {
        throw error;
      }
    }
    return super.set(redisKey, jsonValue, 'EX', timeToLive);
  }

  setSync(group, key, value, timeToLive) {
    return (callback) => {
      try {
        let jsonValue = JSON.stringify(value);
        let redisKey = this.getRedisKey(group, key);
        if (!timeToLive || isNaN(timeToLive)) {
          let error = new Error();
          logger.codeString(error, errorCodes.TTL_IS_MANDATORY);
          if (processEnv === 'PRODUCTION') {
            timeToLive = DEFAULT_CACHE_TTL;
            logger.error(error);
          } else {
            return callback(error);
          }
        }
        super.set(redisKey, jsonValue, 'EX', timeToLive)
          .then(function () {
            callback();
          })
          .catch(function (error) {
            callback(error);
          });
      } catch (error) {
        logger.codeString(error, errorCodes.REDIS_ERROR);
        callback(error);
      }
    };
  }

  invalidateSync(group, key) {
    return (callback) => {
      let redisKey = this.getRedisKey(group, key);
      super.del(redisKey)
        .then(function () {
          callback(null);
        })
        .catch(function (error) {
          logger.codeString(error, errorCodes.REDIS_ERROR);
          callback(error);
        });
    };
  }

  invalidate(group, key) {
    let redisKey = this.getRedisKey(group, key);
    return super.del(redisKey);
  }

  bulkGet(groupsAndKeysArray) {
    return (callback) => {
      try {
        let bulkResult = [];
        for (let groupAndKey of groupsAndKeysArray) {
          let redisKey = this.getRedisKey(groupAndKey.group, groupAndKey.key);
          super.get(redisKey, function (error, result) {
            if(result) {
              bulkResult.push(JSON.parse(result));
            } else {
              bulkResult.push(null);
            }
          });
        }
        callback(null, bulkResult);

      } catch (error) {
        logger.codeString(error, errorCodes.REDIS_ERROR);
        return callback(error);
      }
    };
  }

  bulkSet(groupsAndKeysAndValuesArray) {
    return (callback) => {
      try {
        for (let groupAndKeyAndValue of groupsAndKeysAndValuesArray) {
          let redisKey = this.getRedisKey(groupAndKeyAndValue.group, groupAndKeyAndValue.key);
          let value = groupAndKeyAndValue.value;
          let ttl = groupAndKeyAndValue.ttl;
          if (!ttl || isNaN(ttl)) {
            let error = new Error();
            logger.codeString(error, errorCodes.TTL_IS_MANDATORY);
            if (processEnv === 'PRODUCTION') {
              ttl = DEFAULT_CACHE_TTL;
              logger.error(error);
            } else {
              return callback(error);
            }
          }
          super.set(redisKey, JSON.stringify(value), 'EX', ttl);
        }
        callback();
        
      } catch (error) {
        error.codeString(error, errorCodes.REDIS_ERROR);
        return callback(error);
      }
    };
  }
}

module.exports = Cluster;
