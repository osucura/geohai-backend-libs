module.exports = {
  logger: {
    "loggerName": "CACHE",
    "consoleStream": false,
    "logStashStream": false,
    "logStashLogLevel": "warn",
    "fileStream": true,
    "fileLogLevel": "warn",
    "rotating": true,
    "rotatingPeriod": "1d",
    "rotatingFilesCount": 90,
    "fileName": "/home/gitlab-runner/application-logs/cache.log",
    "financeFileName": "/home/gitlab-runner/application-logs/finance.log",
  },
};
