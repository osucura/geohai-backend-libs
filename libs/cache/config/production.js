module.exports = {
  logger: {
    "loggerName": "CACHE",
    "consoleStream": false,
    "logStashStream": false,
    "logStashLogLevel": "warn",
    "fileStream": true,
    "fileLogLevel": "warn",
    "rotating": true,
    "rotatingPeriod": "1d",
    "rotatingFileCount": 90,
    "fileName": "cache.log",
    "financeFileName": "finance.log",
  },
};
