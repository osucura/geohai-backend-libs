'use strict';
var logger = require('./util/logger');
var crypto = require('crypto');
var max32BitInt = 2147483647;

var strings = {
  generateConfirmationCode: function(codeLength) {
    var chars = '0123456789';
    var code = '';
    for (var i = 0; i < codeLength; i++) {
      var rnum = Math.floor(Math.random() * chars.length);
      code += chars.substring(rnum, rnum + 1);
    }
    return code;
  },
  hashMd5: function(value) {
    if(!value) {
      return;
    }
    var md5sum = crypto.createHash('md5');
    var hashed = md5sum.update(value);
    var digested = hashed.digest('hex');
    return digested;
  },
  hashSHA1: function(value) {
    if(!value) {
      return;
    }
    var sha1 = crypto.createHash('sha512');
    var hashed = sha1.update(value);
    var digested = hashed.digest('hex');
    return digested;
  },
  generateKey: function(timestamp, keyLength) {
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    var key = '';
    for (var i = 0; i < keyLength; i++) {
      var rnum = Math.floor(Math.random() * chars.length);
      key += chars.substring(rnum, rnum + 1);
    }
    var timeInMs = timestamp? (Date.now() + '-') : '';
    return timeInMs + key;
  },
  generateIntKey: function() {
    let key = Math.round(Math.random() * max32BitInt);
    return key;
  },
  convertNumbersToPersian: function(str) {
    let result = '';
    if ( !str && str !== 0 ) {
      return result;
    }
    str = str.toString();
    for(let i = 0; i < str.length; i++) {
      result += str[i] === '1'? '۱' :
        str[i] === '١'? '۱' :
          str[i] === '2'? '۲' :
            str[i] === '٢'? '۲' :
              str[i] === '3'? '۳' :
                str[i] === '٣'? '۳' :
                  str[i] === '4'? '۴' :
                    str[i] === '٤'? '۴' :
                      str[i] === '5'? '۵' :
                        str[i] === '٥'? '۵' :
                          str[i] === '6'? '۶' :
                            str[i] === '٦'? '۶' :
                              str[i] === '7'? '۷' :
                                str[i] === '٧'? '۷' :
                                  str[i] === '8'? '۸' :
                                    str[i] === '٨'? '۸' :
                                      str[i] === '9'? '۹' :
                                        str[i] === '٩'? '۹' :
                                          str[i] === '0'? '۰' :
                                            str[i] === '٠'? '۰' : str[i];
    }
    return result;
  },
  convertNumbersToEnglish: function(str) {
    let result = '';
    if ( !str && str !== 0 ) {
      return result;
    }
    str = str.toString();
    for(let i = 0; i < str.length; i++) {
      result += str[i] === '۱'? '1' :
        str[i] === '١'? '1' :
          str[i] === '۲'? '2' :
            str[i] === '٢'? '2' :
              str[i] === '۳'? '3' :
                str[i] === '٣'? '3' :
                  str[i] === '۴'? '4' :
                    str[i] === '٤'? '4' :
                      str[i] === '۵'? '5' :
                        str[i] === '٥'? '5' :
                          str[i] === '۶'? '6' :
                            str[i] === '٦'? '6' :
                              str[i] === '۷'? '7' :
                                str[i] === '٧'? '7' :
                                  str[i] === '۸'? '8' :
                                    str[i] === '٨'? '8' :
                                      str[i] === '۹'? '9' :
                                        str[i] === '٩'? '9' :
                                          str[i] === '۰'? '0' :
                                            str[i] === '٠'? '0' : str[i];
    }
    return result;
  },
  convertPlusToUnicode: function (str) {
    let result = '';
    for(let i = 0; i < str.length; i++) {
      result += (str[i] === '+' ) ? '%2B' : str[i];
    }
    return result;
  },
  standardizePhoneNumber: function(phoneNumber) {
    if(phoneNumber) {
      if(phoneNumber.indexOf('0') === 0) {
        phoneNumber = '+98' + phoneNumber.substr(1, phoneNumber.length - 1);
      } else if(phoneNumber.indexOf('98') === 0 && phoneNumber.length === 12){
        phoneNumber = '+' + phoneNumber;
      } else if(phoneNumber.indexOf('+98') !== 0){
        phoneNumber = '+98' + phoneNumber;
      }
    }
    return phoneNumber;
  },
  standardizeAllPhoneNumberFormat(phoneNumber) {
    if(phoneNumber) {
      phoneNumber = phoneNumber.split(' ').join('');
      phoneNumber = strings.convertNumbersToEnglish(phoneNumber);
      phoneNumber = strings.standardizePhoneNumber(phoneNumber);
    }
    return phoneNumber;
  },
  convertToZeroPhoneNumber: function(phoneNumber) {
    if(phoneNumber) {
      if(phoneNumber.indexOf('+98') === 0) {
        phoneNumber = '0' + phoneNumber.substr(3, phoneNumber.length - 1);
      } else if(phoneNumber.indexOf('0') !== 0){
        phoneNumber = '0' + phoneNumber;
      }
    }
    return phoneNumber;
  },
  toPersian: function (str) {
    if (str) {
      str = str.replace(/ي/g, 'ی');
      str = str.replace(/ك/g, 'ک');
    }
    return str;
  },
  format: function() {
    let s = arguments[0];
    for (let i = 1; i < arguments.length; i++) {
      let reg = new RegExp('\\{' + (i-1) + '\\}', 'gm');
      s = s.replace(reg, arguments[i]);
    }
    return s;
  },
  clone: function (obj) {
    return (JSON.parse(JSON.stringify(obj)));
  },
  isNumber: function (str) {
    let number = parseInt(str);
    return number && number >= 1 && number <= 10;
  },
  convertPhoneNumberToStandardFormat: function(phoneNumber) {
    phoneNumber = phoneNumber.replace('+', '');
    let convertedPhoneNum;
    let indexOf98 = phoneNumber.indexOf('98');
    if (indexOf98 < 0 || indexOf98 > 0) {
      if (phoneNumber.indexOf('0') === 0) {
        convertedPhoneNum = '98' + phoneNumber.substring(1);
      } else {
        convertedPhoneNum = '98' + phoneNumber;
      }
    } else if (phoneNumber.indexOf('0') === 0) {
      convertedPhoneNum = phoneNumber.substring(1);
    } else {
      convertedPhoneNum = phoneNumber;
    }
    return convertedPhoneNum;
  },
  convertPriceToDelimeteredEnglishString: function (price) {
    let ensuredPrice = Number(price);
    return ensuredPrice.toLocaleString('en-US');
  },
  convertPriceToDelimetedPersianString: function (price) {
    const delimited = price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return `${strings.convertNumbersToPersian(delimited)} تومان`;
  },
  convertPriceToPersianString: function (price) {
    const priceInKT = Math.round(price/1000);
    return `${strings.convertNumbersToPersian(priceInKT)} هزار تومان`;
  },
  convertStringSpecialCharactersToEnglish: function(str) {
    let result = '';
    for(let i = 0; i < str.length; i++) {
      result += str[i] === '٬'? ',' : str[i];
    }
    return result;
  },
  convertStringSpecialCharactersToPersian: function(str) {
    let result = '';
    for(let i = 0; i < str.length; i++) {
      result += str[i] === ','? '٬' : str[i];
    }
    return result;
  },
  fillParameters(str, parameters) {
    const keys = Object.keys(parameters);
    keys.forEach((key) => {
      str = str && str.replace(`%${key}%`, parameters[key])
    });
    return str;
  }
};

module.exports = strings;
