process.env.NODE_ENV = 'TEST';
process.env.CLUSTER_NAME = 'A';

const strings = require('../strings');

describe('strings', function () {
  it('should get the right english text from price integer', function (){
    expect(strings.convertPriceToDelimeteredEnglishString(123)).toBe('123');
    expect(strings.convertPriceToDelimeteredEnglishString(0)).toBe('0');
    expect(strings.convertPriceToDelimeteredEnglishString(15000)).toBe('15,000');
    expect(strings.convertPriceToDelimeteredEnglishString(9800)).toBe('9,800');
    expect(strings.convertPriceToDelimeteredEnglishString(155500)).toBe('155,500');
    expect(strings.convertPriceToDelimeteredEnglishString(25500)).toBe('25,500');
  });

  it('should convert persian text containing special chars to english', function (){
    expect(strings.convertStringSpecialCharactersToEnglish('۱۹٬۲۳۳')).toBe('۱۹,۲۳۳');
    expect(strings.convertStringSpecialCharactersToEnglish('۱۹٬۵۰۰')).toBe('۱۹,۵۰۰');
    expect(strings.convertStringSpecialCharactersToEnglish('۱٬۲۴۲٬۵۳۵')).toBe('۱,۲۴۲,۵۳۵');
    expect(strings.convertStringSpecialCharactersToEnglish('۱۴')).toBe('۱۴');
    expect(strings.convertStringSpecialCharactersToEnglish('۱۵۵')).toBe('۱۵۵');
    expect(strings.convertStringSpecialCharactersToEnglish('۱۴۱٬۱۴۱٬۱۴۱٬۲۲۲')).toBe('۱۴۱,۱۴۱,۱۴۱,۲۲۲');
  });

  it('should convert english text containing special chars to persian', function (){
    expect(strings.convertStringSpecialCharactersToPersian('۱۹,۲۳۳')).toBe('۱۹٬۲۳۳');
    expect(strings.convertStringSpecialCharactersToPersian('۱۹,۵۰۰')).toBe('۱۹٬۵۰۰');
    expect(strings.convertStringSpecialCharactersToPersian('۲۴۲,۵۳۵')).toBe('۲۴۲٬۵۳۵');
    expect(strings.convertStringSpecialCharactersToPersian('۱۴')).toBe('۱۴');
    expect(strings.convertStringSpecialCharactersToPersian('۱,۵۵')).toBe('۱٬۵۵');
    expect(strings.convertStringSpecialCharactersToPersian('۱۴۱3535,۱۴۱,۱۴۱,۲۲۲')).toBe('۱۴۱3535٬۱۴۱٬۱۴۱٬۲۲۲');
  });
});
