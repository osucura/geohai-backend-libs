module.exports = {
  logger: {
    loggerName: 'STRINGS',
    consoleStream: false,
    logStashStream: false,
    logStashLogLevel: 'warn',
    fileStream: true,
    fileLogLevel: 'warn',
    rotating: true,
    rotatingPeriod: '1d',
    rotatingFileCount: 90,
    fileName: 'strings.log',
  },
};
