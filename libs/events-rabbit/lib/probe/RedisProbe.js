const os = require('os');
const Promise = require('bluebird');

const NODE_ENV = process.env.NODE_ENV;
const CLUSTER_NAME = process.env.CLUSTER_NAME;

class RedisProbe {
  constructor(service, redis, rate = 0.0001) {
    if (NODE_ENV === 'PRODUCTION') {
      this.service = service;
      this.hostname = os.hostname();
      this.instance = CLUSTER_NAME;
      this.redis = redis;
      this.rate = rate;
      this._readRateConfig();
    }
  }

  async _readRateConfig() {
    try {
      const rate = await this.redis.get('t30.msg.conf.rate');
      if (rate) {
        this.rate = parseFloat(rate);
      }
      await Promise.delay(5 * 60 * 1000);
      await this._readRateConfig();
    } catch(error) {
      console.log(error);
    }
  }

  async timing(queue, start, end) {
    try {
      if (!this.service) {
        return;
      }
      const metric = `h.${this.hostname}.s.${this.service}.i.${this.instance}.q.${queue}`;
      if (Math.random() < this.rate) {
        await this.redis.hset('t30.msg', metric, `${Date.now()}#${end - start}`);
      }
    } catch (error) {
      console.log(error);
    }
  }
}

module.exports = RedisProbe;
