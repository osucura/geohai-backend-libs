const ioredis = require('ioredis');

const NODE_ENV = process.env.NODE_ENV;

let redis;
if (NODE_ENV === 'PRODUCTION') {
  redis = new ioredis.Cluster([
    {
      port: 9000,
      host: 'redis-amsterdam.esign.server',
    },
    {
      port: 9000,
      host: 'redis-baltimore.esign.server',
    },
    {
      port: 7000,
      host: 'redis-baltimore.esign.server',
    },
    {
      port: 9000,
      host: 'redis-casablanca.esign.server',
    },
    {
      port: 9001,
      host: 'redis-casablanca.esign.server',
    },
  ]);
}

module.exports = redis;
