/**
 * Abstract class to implement a RPC module
 * @interface
 */
class Rpc {
  /**
   * Initializes RPC object
   * @abstract
   * @async
   */
  async init() {
    throw new Error('Not Implemented!');
  }

  /**
   * Binds queue name and handler.
   * Calls handler whenever there is a new message on the specified queue
   * @abstract
   * @param {string} queueName Queue name to listen on.
   * @param {function} handler Function to call upon receiving a new message.
   * @param {boolean} autoDelete Option for queues.
   */
  addHandler(queueName, handler, autoDelete) {
    throw new Error('Not Implemented!');
  }

  /**
   * Initiates a new RPC call to specified queue and waits for answer
   * @abstract
   * @async
   * @param {string} queueName Queue name to send message to.
   * @param {object} data Data to send as message payload
   */
  async call(queueName, data) {
    throw new Error('Not Implemented!');
  }

  /**
   * Initiates multiple RPC calls in parallel to specified queues and waits for answers
   * @abstract
   * @async
   * @param {array} messages Array of objects containing queue names and payload data
   */
  async parallel(messages) {
    throw new Error('Not Implemented!');
  }

  /**
   * Initiates a new RPC call to specified queue and does not wait for answer
   * @abstract
   * @param {string} queueName Queue name to send message to.
   * @param {object} data Data to send as message payload
   */
  send(queueName, data) {
    throw new Error('Not Implemented!');
  }
}

module.exports = Rpc;
