const amqp = require('amqplib');
const msgpack = require('msgpack-lite');
const os = require('os');
const Promise = require('bluebird');
const uuid = require('uuid/v1');
const zlib = require('zlib');
const Rpc = require('./Rpc');
const GraphiteProbe = require('../probe/GraphiteProbe');


const randomConsumerTag = () => {
  return `${os.hostname()}-${uuid()}`;
};

const compressMessage = (data) => {
  return zlib.gzipSync(msgpack.encode(JSON.stringify(data)));
};

const decompressMessage = (compressedData) => {
  return JSON.parse(msgpack.decode(zlib.unzipSync(compressedData)));
};
/**
 * RabbitRpc class, implements RPC module over RabbitMQ
 * @class
 * @implements {Rpc}
 */
class RabbitRpc extends Rpc {
  /**
   * Creates a RabbitRpc object.
   * @constructor
   */
  constructor() {
    super();
    this.responseHandlers = new Map();
  }

  /**
   * Generates a new UUID
   * @private
   * @returns {string} Generated UUID
   */
  static _generateUuid() {
    return Math.random().toString()
      + Math.random().toString()
      + Math.random().toString();
  }

  /**
   * Handler for response queue. Receives messages from response queue and process them.
   * @private
   * @param {object} msg Received message
   */
  _responseHandler(msg) {
    const resolve = this.responseHandlers.get(msg.properties.correlationId);
    if (resolve) {
      resolve(decompressMessage(msg.content));
      this.responseHandlers.delete(msg.properties.correlationId);
    }
  }

  _retryEvent(queueName, content, properties) {
    this.ch.sendToQueue(queueName, content, { ...properties, mandatory: true } );
  }

  _returnHandler(msg) {
    const rabbitRpc = this;
    const resolve = rabbitRpc.responseHandlers.get(msg.properties.correlationId);
    if (resolve) {
      let retried = 0;
      if (msg.properties.headers && msg.properties.headers.retry) {
        retried = parseInt(msg.properties.headers.retry) + 1;
      }
      if (retried > rabbitRpc.retry.count) {
        const error = new Error('NO ROUTING KEY!');
        error.codeString = 'EVENT_ROUTING_PROBLEM';
        error.data = {queueName: msg.fields.routingKey};
        resolve({error});
        this.responseHandlers.delete(msg.properties.correlationId);
      } else {
        msg.properties.headers.retry = String(retried);
        setTimeout(() => {
          rabbitRpc._retryEvent(msg.fields.routingKey, msg.content, msg.properties);
        }, rabbitRpc.retry.timeout);
      }
    }
  }

  // inherits the documentation from `Rpc#init`
  async init(url='amqp://localhost', retry={ timeout: 2000, count: 3 }, service) {
    if (this.ch) {
      return;
    }
    const rabbitRpc = this;
    this.probe = new GraphiteProbe(service);
    this.conn = await amqp.connect(url);
    this.ch = await this.conn.createChannel();
    this.ch.prefetch(20);
    this.retry = retry;
    this.ch.on('return', (msg) => {
      rabbitRpc._returnHandler(msg);
    });
    this.resQueue = await this.ch.assertQueue('', { exclusive: true });
    this.ch.consume(this.resQueue.queue, (msg) => {
      this._responseHandler(msg);
    }, { noAck: true, consumerTag: randomConsumerTag() });
  }

  config() {
    return this;
  }

  // inherits the documentation from `Rpc#addHandler`
  addHandler(queueName, handler, autoDelete=false) {
    this.ch.assertQueue(queueName, { durable: false, autoDelete: autoDelete });
    console.log(` [${queueName}] Awaiting RPC requests `);
    const probeQueueName = queueName.split('.').join('_');

    this.ch.consume(queueName, async (msg) => {
      let result;
      try {
        const message = decompressMessage(msg.content);
        const start = Date.now();
        result = await handler(message);
        const end = Date.now();
        this.probe.timing(probeQueueName, start, end);
      } catch(error) {
        result = {
          error: {
            codeString: 'UNKNOWN_ERROR',
          },
        };
      }
      if (msg.properties.replyTo) {
        this.ch.sendToQueue(msg.properties.replyTo,
          compressMessage(result),
          { correlationId: msg.properties.correlationId });
      }

      this.ch.ack(msg);
    }, {consumerTag: randomConsumerTag()});
  }

  // inherits the documentation from `Rpc#addHandler`
  async addAsyncHandler(queueName, handler, autoDelete = false) {
    const ex = await this.ch.assertExchange(`shard-${queueName}`, 'x-modulus-hash', { durable: true, autoDelete: autoDelete });
    console.log(` [${queueName}] Awaiting RPC requests `);
    const probeQueueName = queueName.split('.').join('_');

    this.ch.consume(ex.exchange, async (msg) => {
      let result;
      try {
        const content = decompressMessage(msg.content);
        if (msg.properties.replyTo) {
          const _job = {
            replyTo: msg.properties.replyTo,
            correlationId: msg.properties.correlationId,
            start: Date.now(),
            probeQueueName,
          };
          if (!content._jobs) {
            content._jobs = [];
          }
          content._jobs.push(_job);
        }
        handler(content);
      } catch(error) {
        result = {
          error: {
            codeString: 'UNKNOWN_ERROR',
          },
        };
        if (msg.properties.replyTo) {
          this.ch.sendToQueue(msg.properties.replyTo,
            compressMessage(result),
            { correlationId: msg.properties.correlationId });
        }
      }

      this.ch.ack(msg);
    }, {consumerTag: randomConsumerTag()});
  }

  /**
   * Same as addHandler function, but the eventName is separated with ":" instead of ".".
   * @deprecated
   * @see addHandler
   * @param {string} eventName Event name to listen on.
   * @param {function} handler Function to call upon receiving a new message.
   */
  addEvent(eventName, handler) {
    const queueName = eventName.split(':').join('.');
    this.addHandler(queueName, handler);
  }

  /**
   * Same as addHandler function, but the eventName is separated with ":" instead of ".".
   * @deprecated
   * @see addHandler
   * @param {string} eventName Event name to listen on.
   * @param {function} handler Function to call upon receiving a new message.
   */
  addAsyncEvent(eventName, handler) {
    const queueName = eventName.split(':').join('.');
    this.addAsyncHandler(queueName, handler);
  }

  // inherits the documentation from `Rpc#call`
  async call(queueName, data, timeoutOptions) {
    const correlationId = RabbitRpc._generateUuid();
    const rabbitRpc = this;
    let expiration;
    const promise = new Promise((resolve) => {
      rabbitRpc.responseHandlers.set(correlationId, resolve);
      if (timeoutOptions && timeoutOptions.timeout) {
        expiration = String((new Date()).getTime() + timeoutOptions.timeout);
        setTimeout(() => {
          const resolve = this.responseHandlers.get(correlationId);
          if (resolve) {
            if (timeoutOptions.default) {
              resolve(timeoutOptions.default);
            } else {
              const error = new Error('JOB TIME OUT!');
              error.codeString = 'EVENT_TIMEOUT';
              error.data = {queueName, timeoutOptions};
              console.log(error);
              resolve({ error });
              rabbitRpc.responseHandlers.delete(correlationId);
            }
          }
        }, timeoutOptions.timeout);
      }
    });

    this.ch.sendToQueue(queueName,
      compressMessage(data),
      { correlationId, replyTo: this.resQueue.queue, expiration, mandatory: true });

    return promise;
  }

  // inherits the documentation from `Rpc#parallel`
  async parallel(messages) {
    const rabbitRpc = this;
    return Promise.map(messages, (message) => {
      return rabbitRpc.call(message.queueName, message.data, message.timeoutOptions);
    });
  }

  // inherits the documentation from `Rpc#send`
  send(queueName, data) {
    const correlationId = RabbitRpc._generateUuid();

    this.ch.sendToQueue(queueName,
      compressMessage(data),
      { correlationId });
  }

  /**
   * Creates queue name from an event object.
   * @private
   * @deprecated
   * @param {object} event Event object
   * @returns {string} Generated UUID
   */
  static _getQueueName(event) {
    let queueName;

    if(event.attribute) {
      queueName = `${event.model}.${event.attribute}.${event.action}`;
    } else {
      queueName = `${event.model}.${event.action}`;
    }

    return queueName;
  }

  /**
   * Same as send function, but the event name and data in combined in an object.
   * @deprecated
   * @see send
   * @param {object} eventData Object containing event name and data
   */
  publishCommand(eventData) {
    const queueName = RabbitRpc._getQueueName(eventData);
    this.send(queueName, eventData.message);
  }

  /**
   * Publish result of a RPC call if handler is async
   * @deprecated
   * @param {object} eventData Object containing event name and data
   */
  publishEvents(eventData) {
    const { message } = eventData;
    if (message._jobs) {
      const _job = message._jobs.pop();
      if (_job) {
        const end = Date.now();
        this.probe.timing(_job.probeQueueName, _job.start, end);
        if (_job.replyTo) {
          this.ch.sendToQueue(_job.replyTo,
            compressMessage(message),
            { correlationId: _job.correlationId });
        }
      }
    }
  }

  /**
   * Same as call function, but the event name and data in combined in an object.
   * @deprecated
   * @see call
   * @async
   * @param {object} eventData Object containing event name and data
   */
  async publishAndReceiveEvent(eventData) {
    const queueName = RabbitRpc._getQueueName(eventData);
    return this.call(queueName, eventData.message, eventData.timeoutOptions);
  }

  /**
   * Same as parallel function, but it calls multiple events and waits for response
   * @deprecated
   * @see parallelCall
   * @async
   * @param {object} eventData Object containing event name and data
   */
  async publishAndReceiveEventsAsync(eventData) {
    const messages = [];

    for (const event of eventData.events) {
      const message = {
        queueName: RabbitRpc._getQueueName(event),
        data: event.message,
        timeoutOptions: event.timeoutOptions,
      };
      messages.push(message);
    }

    return this.parallel(messages);
  }

  /**
   * Same as parallel function, but it calls multiple events and waits for response
   * @deprecated
   * @see parallelCall
   * @param {object} eventData Object containing event name and data
   */
  publishAndReceiveEvents(eventData) {
    const rabbitRpc = this;
    return (callback) => {
      rabbitRpc.publishAndReceiveEventsAsync(eventData)
        .then(results => {
          for (let i = 0; i < results.length; i += 1) {
            eventData.events[i].result = results[i];
          }
          callback(null, eventData);
        })
        .catch(error => {
          callback(error);
        });
    };
  }

  /**
   * Does nothing
   * @deprecated
   * @param {object} eventData Object containing event name and data
   */
  registerEvents() {
  }
}

module.exports = RabbitRpc;
