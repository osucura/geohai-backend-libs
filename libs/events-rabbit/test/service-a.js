const client = require('../index');

const run = async () => {
  try {
    await client.init();
    client.addEvent('a:c', (message) => {
      console.log(`${message} -1`);
      message.response = 'hello from a:c 1';
      return message;
    });

    client.addEvent('eta:etaAndDistance:estimate', (message) => {
      console.log(`${message} - 2`);
      message.response = 'hello from a:b:c 2';
      return message;
    });
  } catch (error) {
    console.error(error);
    setTimeout(() => {
      process.exit(1);
    }, 5000);
  }
};

run();
