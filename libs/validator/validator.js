const errorCodes = require('./config/errorCodes.json');

const privates = {
  isNullOrEmpty(value) {
    return value === undefined || value === null || (typeof value === 'string' && value.trim() === '');
  },
};

const functions = {
  isNumber(value, fieldName) {
    if (isNaN(value)) {
      const error = new Error();
      error.data = { fieldName };
      error.codeString = errorCodes.INVALID_VALUE;
      throw error;
    }
  },
  isBoolean(value, fieldName) {
    if (typeof (value) !== 'boolean') {
      const error = new Error();
      error.data = { fieldName };
      error.codeString = errorCodes.INVALID_VALUE;
      throw error;
    }
  },
  isNullOrEmpty(value, fieldName) {
    if (value !== undefined && value !== null && (typeof value === 'string' && value.trim() !== '')) {
      const error = new Error();
      error.data = { fieldName };
      error.codeString = errorCodes.INVALID_VALUE;
      throw error;
    }
  },
  notNullOrEmpty(value, fieldName) {
    if (typeof value === 'boolean' && value !== null) {
      return;
    }
    if (value === undefined || value === null || (typeof value === 'string' && value.trim() === '')) {
      const error = new Error();
      error.codeString = errorCodes.MISSING_FIELD;
      error.data = { fieldName };
      throw error;
    }
  },
  notNullOrEmptyBoth(value1, value2, fieldName) {
    if ((typeof value1 === 'boolean' && value1 !== null) || (typeof value2 === 'boolean' && value2 !== null)) {
      return;
    }
    if (
      (
        value1 === undefined ||
        value1 === null ||
        (typeof value1 === 'string' && value1.trim() === '')
      ) &&
      (
        value2 === undefined ||
        value2 === null ||
        (typeof value2 === 'string' && value2.trim() === '')
      )
    ) {
      const error = new Error();
      error.data = {fieldName};
      error.codeString = errorCodes.MISSING_FIELD;
      throw error;
    }
  },
  onlyOneExists(values, fieldnames) {
    const existingValues = values.filter(value => !privates.isNullOrEmpty(value));
    if (existingValues.length !== 1) {
      const error = new Error();
      error.data = {...fieldnames};
      error.codeString = errorCodes.ONLY_ONE_IS_ACCEPTED;
      throw error;
    }
  },
  possibleValues(value, possibleValues, fieldName) {
    if (possibleValues && possibleValues.indexOf(value) === -1) {
      const error = new Error();
      error.data = { fieldName};
      error.codeString = errorCodes.INVALID_VALUE;
      throw error;
    }
  },
  startsWith(value, str) {
    if (!value.startsWith(str)) {
      const error = new Error();
      error.data = { value };
      error.codeString = errorCodes.INVALID_VALUE;
      throw error;
    }
  },
  locationOrUndefined(value, fieldName) {
    if (!value) {
      return;
    }
    if (
      (!value.longitude && value.longitude !== 0) ||
      (!value.latitude && value.latitude !== 0) ||
      typeof value.longitude !== 'number' ||
      typeof value.latitude !== 'number'
    ) {
      const error = new Error();
      error.data = { fieldName: fieldName || 'location'};
      error.codeString = errorCodes.INVALID_VALUE;
      throw error;
    }
    if (value.longitude < -180 || value.longitude > 180) {
      const error = new Error();
      error.data = { fieldName: `longitude${fieldName ? (`-${fieldName}`) : ''}`};
      error.codeString = errorCodes.INVALID_VALUE;
      throw error;
    }
    if (value.latitude < -85.05112878 || value.latitude > 85.05112878) {
      const error = new Error();
      error.data = { fieldName: `latitude${fieldName ? (`-${fieldName}`) : ''}`};
      error.codeString = errorCodes.INVALID_VALUE;
      throw error;
    }
  },
  location(value, fieldName) {
    functions.notNullOrEmpty(value, fieldName);
    if (
      (!value.longitude && value.longitude !== 0) ||
      (!value.latitude && value.latitude !== 0) ||
      typeof value.longitude !== 'number' ||
      typeof value.latitude !== 'number'
    ) {
      const error = new Error();
      error.data = { fieldName: fieldName || 'location'};
      error.codeString = errorCodes.INVALID_VALUE;
      throw error;
    }
    if (value.longitude < -180 || value.longitude > 180) {
      const error = new Error();
      error.data = { fieldName: `longitude${fieldName ? (`-${fieldName}`) : ''}`};
      error.codeString = errorCodes.INVALID_VALUE;
      throw error;
    }
    if (value.latitude < -85.05112878 || value.latitude > 85.05112878) {
      const error = new Error();
      error.data = { fieldName: `latitude${fieldName ? (`-${fieldName}`) : ''}`};
      error.codeString = errorCodes.INVALID_VALUE;
      throw error;
    }
  },
  mustBeInRange(value, fieldName, min, max) {
    if ((min && value < min) || (max && value > max)) {
      const error = new Error();
      error.data = { fieldName};
      error.codeString = errorCodes.INVALID_VALUE;
      throw error;
    }
  },
  date(date, fieldName) {
    if (isNaN(Date.parse(date))) {
      const error = new Error();
      error.data = { fieldName: fieldName || 'date'};
      error.codeString = errorCodes.INVALID_DATE;
      throw error;
    }
  },
  valueBeforeValue(firstValue, secondValue, fieldName) {
    if (firstValue >= secondValue) {
      const error = new Error();
      error.data = { fieldName: fieldName || 'firstValue'};
      error.codeString = errorCodes.INVALID_DATE;
      throw error;
    }
  },
  email(value) {
    const validEmails = /^\S+@\S+\.\S+$/;
    if (!validEmails.test(value)) {
      const error = new Error();
      error.data = { fieldName: 'email'};
      error.codeString = errorCodes.INVALID_VALUE;
      throw error;
    }
  },
  image(image) {
    const fileFormats = image.originalname.split('.');
    const imageFormat = fileFormats[fileFormats.length - 1];
    const validImageFormats = ['jpg', 'png', 'jpeg'];
    if (validImageFormats.indexOf(imageFormat.toLowerCase()) === -1) {
      const error = new Error();
      error.data = { fieldName: 'image format'};
      error.codeString = errorCodes.INVALID_VALUE;
      throw error;
    }
  },
  notEmptyArray(values, fieldName) {
    functions.notNullOrEmpty(values, fieldName);
    if (typeof values === 'object' && values.constructor === Array && values.length > 0) {
      return;
    }
    const error = new Error();
    error.data = { fieldName};
    error.codeString = errorCodes.MISSING_FIELD;
    throw error;
  },
  notEmptyJson(value, fieldName) {
    if (value && Object.keys(value).length > 0) {
      return;
    }
    const error = new Error();
    error.data = { fieldName};
    error.codeString = errorCodes.MISSING_FIELD;
    throw error;
  },
  atLeastOneShouldNotBeNullOrEmpty(values, fieldName) {
    for (let i = 0; i < values.length; i++) {
      if (typeof values[i] === 'string' && values[i].trim() !== '') {
        return;
      }
      if (values[i] !== undefined && values[i] !== null) {
        return;
      }
    }

    const error = new Error();
    error.data = { fieldName};
    error.codeString = errorCodes.MISSING_FIELD;
    throw error;
  },
  arrayMinLength(arr, minLen, fieldName) {
    if (arr && arr.length < minLen) {
      const error = new Error();
      error.data = {fieldName, arr, minLen};
      error.codeString = errorCodes.INSUFFICIENT_ARGUMENTS;
      throw error;
    }
  },
  arrayMaxLength(arr, maxLen, fieldName) {
    if (arr && arr.length > maxLen) {
      const error = new Error();
      error.data = {fieldName, arr, maxLen};
      error.codeString = errorCodes.INVALID_VALUE;
      throw error;
    }
  },
  objectMustHaveIndex(idx, obj, fieldName, codeString, extraParams) {
    if (obj && (idx in obj) === false) {
      const error = new Error();
      error.data = {fieldName, idx, extraParams};
      error.codeString = codeString || errorCodes.INVALID_VALUE;
      throw error;
    }
  },
  isFunction(value, fieldName, codeString) {
    const getType = {};
    if (!(value && (getType.toString.call(value) === '[object Function]') || getType.toString.call(value) === '[object AsyncFunction]')) {
      const error = new Error();
      error.data = { fieldName};
      error.codeString = codeString || errorCodes.INVALID_VALUE;
      throw error;
    }
  },
  isObject(value, fieldName, codeString) {
    if (value !== Object(value) || Object.prototype.toString.call(value) === '[object Array]') {
      const error = new Error();
      error.data = { fieldName};
      error.codeString = codeString || errorCodes.INVALID_VALUE;
      throw error;
    }
  },
};

module.exports = functions;

