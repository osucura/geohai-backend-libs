const { initTracer: initJaegerTracer } = require('jaeger-client');

module.exports.initTracer = (serviceName, host, port) => {
  const config = {
    serviceName,
    sampler: {
      type: 'probabilistic',
      param: 0.02,
    },
    reporter: {
      logSpans: true,
      agentHost: host,
      agentPort: port,
    },
  };
  const options = {
    logger: {
      info() {},
      error(msg) {
        console.log('JAEGER CLIENT ERROR', msg);
      },
    },
  };
  return initJaegerTracer(config, options);
};
