
const Promise = require('bluebird');
const opentracing = require('opentracing');
const { initTracer } = require('../config/tracing');
const nameSpaceAsyncHook = require('./asynchooks');

const { NODE_ENV, CLUSTER_NAME, JAEGER_AGENT_HOST = 'localhost', JAEGER_AGENT_PORT = 6832 } = process.env;
const nameSpaceLabel = 'jaegertracing';

class JaegerTracing {
  constructor(serviceName, redis) {
    let tracerLabel = serviceName;
    if (CLUSTER_NAME === 'kuber') {
      tracerLabel = `${CLUSTER_NAME} - ${serviceName}`;
    }
    this.tracer = initTracer(tracerLabel, JAEGER_AGENT_HOST, JAEGER_AGENT_PORT);
    this.enable = false;
    this.namespace = null;
    this.redis = redis;
    if (NODE_ENV === 'PRODUCTION') {
      this.checkJaegerTracingStatus();
    }
  }

  async checkJaegerTracingStatus() {
    try {
      const jaegerTracingStatus = await this.redis.get('jaeger_tracing_status');
      if (jaegerTracingStatus !== 'enable' && this.enable) {
        nameSpaceAsyncHook.deleteNameSpace(nameSpaceLabel);
        this.namespace = null;
        this.enable = false;
      }
      if (jaegerTracingStatus === 'enable' && !this.enable) {
        this.namespace = nameSpaceAsyncHook.createNamespace(nameSpaceLabel);
        this.enable = true;
      }
      await Promise.delay(60 * 1000);
      this.checkJaegerTracingStatus();
    } catch (error) {
      console.log(`checkJaegerTracingStatus error: ${error}`);
      await Promise.delay(60 * 1000);
      this.checkJaegerTracingStatus();
    }
  }

  createSpan(label, parent) {
    const parentSpan = parent || this.namespace.get('span');
    if (parentSpan) {
      return this.tracer.startSpan(label, { childOf: parentSpan });
    }
    return this.tracer.startSpan(label);
  }

  handleParentSpan(spanDeserialized) {
    if (!spanDeserialized) {
      return;
    }
    const span = this.deserializeSpan(spanDeserialized);
    this.saveSpan(span);
  }

  saveSpan(span) {
    this.namespace.set('span', span);
  }

  serializeSpan(span) {
    const spanSerialized = {};
    this.tracer.inject(span, opentracing.FORMAT_TEXT_MAP, spanSerialized);
    return spanSerialized;
  }

  deserializeSpan(spanSerialized) {
    const span = this.tracer.extract(opentracing.FORMAT_TEXT_MAP, spanSerialized);
    return span;
  }
}

module.exports = JaegerTracing;
