const asyncHooks = require('async_hooks');
const Namespace = require('./namespace');

const namespaces = {};
const hooks = {};

function createHooks(namespace) {
  function init(asyncId, type, triggerId, resource) {
    if (namespace.context[triggerId]) {
      namespace.context[asyncId] = namespace.context[triggerId];
    }
  }

  function destroy(asyncId) {
    delete namespace.context[asyncId];
  }

  const asyncHook = asyncHooks.createHook({ init, destroy });

  return asyncHook;
}

function createNamespace(name) {
  if (namespaces[name]) {
    throw new Error(`A namespace for ${name} already exists`);
  }

  const namespace = new Namespace();
  namespaces[name] = namespace;

  const hook = createHooks(namespace);
  hook.enable();
  hooks[name] = hook;

  return namespace;
}

function deleteNameSpace(name) {
  if (hooks[name]) {
    hooks[name].disable();
  }
  delete namespaces[name];
  delete hooks[name];
}

function getNamespace(name) {
  return namespaces[name];
}

module.exports = {
  createNamespace,
  getNamespace,
  deleteNameSpace,
};
