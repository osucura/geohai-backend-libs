const asyncHooks = require('async_hooks');

class Namespace {
  constructor() {
    this.context = {};
  }

  set(key, val) {
    const eid = asyncHooks.executionAsyncId();
    this.context[eid] = this.context[eid] || {};
    this.context[eid][key] = val;
  }

  get(key) {
    const eid = asyncHooks.executionAsyncId();
    if (this.context[eid]) {
      return this.context[eid][key];
    }
    return undefined;
  }
}

module.exports = Namespace;
