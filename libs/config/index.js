const convict = require('convict');
const fs = require('fs');


module.exports = (schema, configDirName) => {
  const config = convict(schema);
  const configEnvironment = config.get('env');
  const configFilePath = `${configDirName}/${configEnvironment.toLowerCase()}.js`;
  fs.accessSync(configFilePath, fs.F_OK);

  const configObject = require(configFilePath);
  config.load(configObject);
  config.validate();

  return config;
};

module.exports.types = require('./types');